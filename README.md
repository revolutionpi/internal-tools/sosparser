# SOS-Parser

## Umgebungsvariablen

#### MY_STORAGE_DIRECTORY

Gibt den Speicherort für die Reports und die Checks an.

Ohne Angabe eines Speicherverzeichnisses wird automatisch der Ordner 
"SOS-Parser" im Home-Verzeichnis des ausführenden Benutzers angelegt. Bei
Ausführung als WSGI-Anwendung auf einem Webserver muss diese Variable gesetzt
werden. Siehe [Beispieldatei](data/apache.conf).

## Projekt bauen

Beim Bauen einer ausführbaren Datei werden automatisch alle Lizenzen der
eingebetteten Module gesammelt und können über den
Parameter `--open-source-licenses` abgerufen werden.

Zusätzlich befinden sich im "dist" Ordner im Projektverzeichnis noch die
Dateien "bundled-libraries.md", "open-source-licenses.json" und
"open-source-licenses.txt", welche ebenfalls alle Lizenzen auflisten.

### Auf Windows für Windows als .exe

Durch die "make.bat" Datei muss auf Windows kein GnuMake (oder ähnliches)
installiert sein. Die "make.bat" hat die benötigten Targets integriert.

> Alle Befehle müssen in der "cmd.exe" ausgefürt werden, nicht in der
> PowerShell!

Durch Abhängigkeiten, welche binäre nur für 64 Bit existieren, sollte eine 64
Bit Pythonversion zwingend verwendet werden.

1) `make venv`: Erstellt die virtuelle Umgebung für das Bauen und Ausführen des
   Projekts. Bei der Einrichtung werden alle Abhängigkeiten installiert.
2) `make app`: Baut in der virtuellen Umgebung die ausführbare Datei. Diese wird
   mit allen Abhängigkeiten und dem Python-Interpreter gepackt und kann auf
   jedem Windowssystem (64 Bit) direkt ausgeführt werden. Die Datei befindet
   sich im Unterordner "dist" des Projekts.

### Auf MacOS und Linux

Im Projektordner befindet sich ein Makefile, welches die benötigten Targets
enthält.

1) `make venv`: Erstellt die virtuelle Umgebung für das Bauen und Ausführen des
   Projekts. Es wird für die Einrichtung der Python-Standardinterpreter aus der
   PATH Variable verwendet. Bei der Einrichtung werden alle Abhängigkeiten
   installiert.
2) `make app`: Baut in der virtuellen Umgebung die ausführbare Datei. Diese wird
   mit allen Abhängigkeiten und dem Python-Interpreter gepackt und kann auf
   jedem vergleichbaren System (Architektur / Version oder neuer) direkt
   ausgeführt werden. Die Datei befindet sich im Unterordner "dist" des
   Projekts.
