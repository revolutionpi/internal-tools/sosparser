# -*- coding: utf-8 -*-
"""Setup-script for SOS Parser."""
# SPDX-FileCopyrightText: 2024 KUNBUS GmbH <support@kunbus.com>
# SPDX-License-Identifier: GPL-2.0-or-later

from setuptools import find_namespace_packages, setup

from src.sosparser.__about__ import __version__

with open("README.md") as fh:
    # Load long description from readme file
    long_description = fh.read()

setup(
    name="sosparser",
    version=__version__,
    packages=find_namespace_packages("src"),
    package_dir={"": "src"},
    include_package_data=True,
    python_requires=">= 3.7",
    install_requires=[
        "flask",
        "pandas",
        "openpyxl",
        "colorama",
    ],
    entry_points={
        "console_scripts": [
            "sosparser = sosparser.main_application:main",
        ],
    },
    platforms=["revolution pi"],
    url="https://revolutionpi.com/",
    license="GPLv2",
    license_files=["LICENSES/*"],
    author="Sven Sager",
    author_email="s.sager@kunbus.com",
    maintainer="KUNBUS GmbH",
    maintainer_email="support@kunbus.com",
    description="SOS report parser for Revolution Pi",
    long_description=long_description,
    long_description_content_type="text/markdown",
    keywords=["revpi", "revolution pi", "plc", "automation"],
    classifiers=[
        # A list of all classifiers: https://pypi.org/pypi?%3Aaction=list_classifiers
        "Development Status :: 4 - Beta",
        # "Development Status :: 5 - Production/Stable",
        "Environment :: Web Environment",
        "Intended Audience :: Manufacturing",
        "License :: OSI Approved :: GNU General Public License v2 or later (GPLv2+)",
        "Operating System :: POSIX",
        "Topic :: Internet :: WWW/HTTP",
    ],
)
