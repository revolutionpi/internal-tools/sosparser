# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText: 2024 KUNBUS GmbH <support@kunbus.com>
# SPDX-License-Identifier: GPL-2.0-or-later
"""Flask app of sos parser app."""
import locale
from os import environ
from os.path import exists
from pathlib import Path
from shutil import copyfile

from flask import Flask, g, request

# Create storage directories
storage_directory = Path(environ["MY_STORAGE_DIRECTORY"])
storage_directory.mkdir(parents=True, exist_ok=True)

upload_directory = storage_directory.joinpath("uploads")
upload_directory.mkdir(exist_ok=True)

checks_directory = storage_directory.joinpath("checks")
checks_directory.mkdir(exist_ok=True)

# Create flask app
app = Flask(__name__)
app.config["STORAGE_DIRECTORY"] = str(storage_directory)
app.config["UPLOAD_DIRECTORY"] = str(upload_directory)
app.config["CHECK_PATTERN"] = str(checks_directory.joinpath("CheckPattern.xlsx"))
app.config["CHECK_VERSION"] = str(checks_directory.joinpath("CheckVersion.xlsx"))

# Copy supplied checks, if not exist
if not exists(app.config["CHECK_PATTERN"]):
    copyfile(
        Path(app.static_folder).joinpath("check_supplied", "CheckPattern.xlsx"),
        app.config["CHECK_PATTERN"],
    )
if not exists(app.config["CHECK_VERSION"]):
    copyfile(
        Path(app.static_folder).joinpath("check_supplied", "CheckVersion.xlsx"),
        app.config["CHECK_VERSION"],
    )


@app.before_request
def before_request() -> None:
    g.language = request.accept_languages.best_match(["de", "en"])
    if g.language == "de":
        locale.setlocale(locale.LC_ALL, "de_DE.UTF-8")


if app:
    # After init app, load all flask modules, which depends on app
    from . import analyze
    from . import browse_report
    from . import home
    from . import search
