# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText: 2024 KUNBUS GmbH <support@kunbus.com>
# SPDX-License-Identifier: GPL-2.0-or-later
"""Helper functions for sos parser."""
import json
from os.path import join
from pathlib import Path
from time import localtime, strftime, time

from . import app


class ReportMetadata:
    """
    Metadata for saved sos reports.

    This class manages the metadata that is stored in addition to a report.
    """

    def __init__(self, report_id: str):
        self._meta_file = Path(
            join(app.config["UPLOAD_DIRECTORY"], report_id, "sosparser-metadata.json"),
        )
        self._report_id = report_id
        self._data = {}

    def __getitem__(self, item):
        return self._data[item]

    def __setitem__(self, key, value):
        self._data[key] = value
        self.save()

    def load(self) -> bool:
        """Load metadata form report directory."""
        try:
            with open(self._meta_file, "r") as fh:
                self._data = json.load(fh)
        except Exception as e:
            return False
        return True

    def save(self) -> bool:
        try:
            with open(self._meta_file, "w") as fh:
                json.dump(self._data, fh, indent=2, sort_keys=True)
        except Exception as e:
            return False
        return True

    def set_upload_ts(self):
        """Set actual time as upload timestamp."""
        self["upload_ts"] = int(time())

    @property
    def replaced(self) -> bool:
        return bool(self._data.get("replaced", False))

    @replaced.setter
    def replaced(self, value):
        self["replaced"] = bool(value)

    @property
    def upload_ts(self) -> int:
        """Get the upload time stamp of report."""
        return int(self._data.get("upload_ts", 0))

    @property
    def upload_time_string(self) -> str:
        local_time = localtime(self.upload_ts)
        return strftime("%Y-%m-%d %H:%M:%S", local_time)
