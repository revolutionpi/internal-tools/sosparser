# SPDX-FileCopyrightText: 2024 KUNBUS GmbH <support@kunbus.com>
# SPDX-License-Identifier: GPL-2.0-or-later
"""Search page for sos reports."""
from collections import namedtuple
from os.path import basename
from pathlib import Path

from flask import render_template

from . import app
from .helper import ReportMetadata

Report = namedtuple("Report", ["id", "upload"])


@app.route("/search")
def search():
    reports = []

    reports_dir = Path(app.config["UPLOAD_DIRECTORY"])
    for inode in reports_dir.iterdir():
        if not inode.is_dir():
            continue

        report_id = basename(inode)
        report_meta = ReportMetadata(report_id)
        report_meta.load()
        reports.append(
            Report(
                report_id,
                report_meta.upload_time_string,
            )
        )

    # Sort reports by upload date, newest first
    reports.sort(key=lambda report: report.upload, reverse=True)

    return render_template(
        "search.html",
        reports=reports,
    )
