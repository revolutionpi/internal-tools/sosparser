# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText: 2024 KUNBUS GmbH <support@kunbus.com>
# SPDX-License-Identifier: GPL-2.0-or-later
"""Browse original files of sos report."""
from os.path import abspath, join
from pathlib import Path

from flask import send_file
from werkzeug.exceptions import BadRequest

from . import app


@app.route("/browse_report/<report_id>/<path:path>")
def browse_report(report_id: str, path):
    """Render report page from original files of sos report."""
    report_directory = Path(join(app.config["UPLOAD_DIRECTORY"], report_id))

    if not report_directory.exists():
        return BadRequest("Can not find report id")

    # Split the url path and rebuild with join to match the right separators (win/unix)
    request_file = report_directory.joinpath(*path.split("/"))
    if abspath(request_file).find(str(report_directory)) != 0:
        return BadRequest("Invalid path")

    # Prepare file to send, this will autodetect mime type
    file_response = send_file(str(request_file))

    if file_response.mimetype == "application/octet-stream":
        # Replace an octet-stream with text/plain, this will be command logs of sos report
        file_response.mimetype = "text/plain"

    return file_response
