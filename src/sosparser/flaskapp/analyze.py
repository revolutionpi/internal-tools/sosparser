# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText: 2024 KUNBUS GmbH <support@kunbus.com>
# SPDX-License-Identifier: GPL-2.0-or-later
"""Analyse the sos report."""
import os
import re
import shutil
import tarfile
from pathlib import Path
from time import time

import pandas as pd
from colorama import Fore, Style
from flask import redirect, render_template, request, url_for

from . import app
from .helper import ReportMetadata

debug = 0


class DataAccessor:
    def __init__(self):
        # Pfad zum aktuellen Verzeichnis
        self.current_path = os.path.dirname(os.path.abspath(__file__))

        # Pfad zur Excel-Datei
        self.file_path_Version = app.config["CHECK_VERSION"]
        self.file_path_check_pattern = app.config["CHECK_PATTERN"]

        # Daten aus der Excel-Datei laden
        self.r_file_path_Version = pd.read_excel(self.file_path_Version)
        self.r_file_path_check_pattern = pd.read_excel(self.file_path_check_pattern)

        # Spalten in separate Variablen speichern
        self.variable = self.r_file_path_Version["variable"].tolist()
        self.version = self.r_file_path_Version["version"].tolist()

        # Beispielhafter Zugriff auf die Variablen
        if debug == 1:
            print(self.variable)
            print(self.version)

    def getIndex(self, variableName):
        return self.variable.index(str(variableName))

    def getVersion(self, index):
        return self.version[index]

    def getFile(self, index):
        return self.file[index]

    def getTest(self, index):
        return self.test[index]

    def getNOK(self, index):
        return self.nok[index]

    def getHint(self, index):
        return self.hint[index]

    def getKnowledge(self, index):
        return self.knowledge[index]

    def getExcelVersionList(self):
        return self.r_file_path_Version

    def getExcelPatterList(self):
        return self.r_file_path_check_pattern


class DataCollector:
    def __init__(self):
        self.vector = []
        self.data_complete = 0
        self.dict = {
            "Test": "void",
            "Pattern": "void",
            "Run": "Fail",
            "NOK": "void",
            "Hint": "void",
            "Knowledge": "void",
        }

    def get_data(self):
        return self.vector

    def add_tuppel(self):
        self.vector.append(self.dict.copy())
        self.reset_dict()

    def set_status(self, status):
        self.data_complete = status

    def reset_dict(self):
        self.dict["Test"] = "void"
        self.dict["Pattern"] = "void"
        self.dict["Run"] = "Fail"
        self.dict["NOK"] = "void"
        self.dict["Hint"] = "void"
        self.dict["Knowledge"] = "void"

    def update_dict(self, key, value):
        self.dict[key] = value

    def delete_data(self):
        self.vector = []


"""----------------------------Variable Area------------------------Begin"""
data = DataAccessor()
dataCollector = DataCollector()

current_kernel_version = data.getVersion(data.getIndex("KernelVersion"))
"""----------------------------Variable Area------------------------End"""


@app.route("/upload_and_extract", methods=["POST"])
def upload_and_extract():
    """Extract uploaded archive to upload storage."""
    uploaded_file = request.files["file"]

    if not uploaded_file:
        return redirect(url_for("home"))

    report_id, *extensions = uploaded_file.filename.split(".")
    report_dir = Path(os.path.join(app.config["UPLOAD_DIRECTORY"], report_id))
    report_meta = ReportMetadata(report_id)

    # Remove existing report directory
    if report_dir.exists():
        report_meta.replaced = True
        shutil.rmtree(report_dir)

    # Datei extrahieren
    with tarfile.open(f"r:{extensions[-1].lower()}", fileobj=uploaded_file.stream) as tar:
        tar.extractall(app.config["UPLOAD_DIRECTORY"])

    report_meta.set_upload_ts()

    return redirect(url_for("analyze_report", report_id=report_id))


@app.route("/analyze/<report_id>")
def analyze_report(report_id: str):
    report_path = Path(os.path.join(app.config["UPLOAD_DIRECTORY"], report_id))
    dataCollector.delete_data()
    pattern_checker(data.getExcelPatterList(), report_path)
    return render_template(
        "analyze.html",
        vector=dataCollector.get_data(),
        report_id=report_id,
    )


def pattern_checker(ExcelPatternList, report_dir):
    # Iteriere über jede Zeile
    for index, row in ExcelPatternList.iterrows():
        if debug == 1:
            print(
                Fore.YELLOW
                + "######################################################################################"
                + Style.RESET_ALL
            )
        if debug == 1:
            # Setze die Farbe für den Text auf Weiß
            print(Fore.WHITE)

            # Variable
            print("variable:", end=" ")
            print(Fore.LIGHTMAGENTA_EX + row[0], end=", ")
            print(Style.RESET_ALL)

            # what to do
            print("what to do:", end=" ")
            print(Fore.LIGHTMAGENTA_EX, row[1], end=", ")
            print(Style.RESET_ALL)

            # content
            print("content:", end=" ")
            print(Fore.LIGHTMAGENTA_EX, row[2], end=", ")
            print(Style.RESET_ALL)

            # File
            print("File:", end=" ")
            print(Fore.LIGHTMAGENTA_EX, row[3], end=", ")
            print(Style.RESET_ALL)

            # Test
            print("Test:", end=" ")
            print(Fore.LIGHTMAGENTA_EX, row[4], end=", ")
            print(Style.RESET_ALL)

            # NOK
            print("NOK:", end=" ")
            print(Fore.LIGHTMAGENTA_EX, row[5], end=", ")
            print(Style.RESET_ALL)

            # Hint
            print("Hint:", end=" ")
            print(Fore.LIGHTMAGENTA_EX, row[6], end=", ")
            print(Style.RESET_ALL)

            # Knowledge
            print("Knowledge:", end=" ")
            print(Fore.LIGHTMAGENTA_EX, row[7], end=", ")
            print(Style.RESET_ALL)

        file_path = os.path.join(report_dir, *row[3].split("\\"))
        if os.path.isfile(file_path):
            with open(file_path, "r") as f:
                text_file = f.read().strip()
                pattern = str(row[4])

            if debug == 1:
                print("Path: ", file_path)
                print(Fore.BLUE, "pattern: ", pattern, Style.RESET_ALL)
                print(Fore.CYAN, "Mode: ", row[1], Style.RESET_ALL)

            dataCollector.update_dict("Test", str(row[0]))
            dataCollector.update_dict("Pattern", str(row[4]))
            dataCollector.update_dict("NOK", str(row[5]))
            dataCollector.update_dict("Hint", str(row[6]))
            dataCollector.update_dict("Knowledge", str(row[7]))

            content = 0

            if row[1] == "cmp":
                if text_file.find(pattern) != -1:
                    if debug == 1:
                        print(
                            Fore.GREEN,
                            "Der Suchstring wurde gefunden bei Index",
                            index,
                            Style.RESET_ALL,
                        )
                    content = 1
                else:
                    if debug == 1:
                        print(
                            Fore.RED,
                            "Der Suchstring wurde nicht gefunden bei Index",
                            index,
                            Style.RESET_ALL,
                        )

            elif row[1] == "re":
                regex = re.compile(pattern)
                match = re.findall(regex, text_file)
                if match:
                    if debug == 1:
                        print(match)
                        print(
                            Fore.GREEN,
                            "Der Suchstring wurde gefunden bei Index",
                            index,
                            Style.RESET_ALL,
                        )
                    content = 1
                else:
                    if debug == 1:
                        print(
                            Fore.RED,
                            "Der Suchstring wurde nicht gefunden bei Index",
                            index,
                            Style.RESET_ALL,
                        )
            else:
                content = -1
                if debug == 1:
                    print("No Fuction selected")

            if debug == 1:
                print(Fore.YELLOW, "check bevor: ", content, Style.RESET_ALL)

            if content == 1 and row[2] == "no":
                content = 0
            elif content == 0 and row[2] == "no":
                content = 1

            if debug == 1:
                print(Fore.YELLOW, "check after: ", content, Style.RESET_ALL)

            if content == 1:
                dataCollector.update_dict("Run", "Success")
            elif content == 0:
                dataCollector.update_dict("Run", "Fail")
            elif content == -1:
                dataCollector.update_dict("Run", "None")

            dataCollector.add_tuppel()

            if debug == 1:
                print(Fore.LIGHTBLUE_EX)
                print("hint: ", row[6])
                print("Knowledge: ", row[7])
                print(Style.RESET_ALL)
        else:
            if debug == 1:
                print(Fore.BLUE, "Keine Angaben2", content, Style.RESET_ALL)
            dataCollector.update_dict("Test", str(row[0]))
            dataCollector.update_dict("Pattern", str(row[4]))
            dataCollector.update_dict("Run", "None")
            dataCollector.add_tuppel()


@app.route("/analyze/<report_id>/modal")
def analyze_report_modal(report_id: str):
    report_meta = ReportMetadata(report_id)
    report_meta.load()

    report_path = Path(os.path.join(app.config["UPLOAD_DIRECTORY"], report_id))
    dataCollector.delete_data()
    pattern_checker(data.getExcelPatterList(), report_path)
    test_results = [
        test for test in dataCollector.get_data() if test["Test"] == request.args.get("test_id")
    ]

    return render_template(
        "analyze_modal.html",
        testresults=test_results,
        upload_date=report_meta.upload_time_string,
    )
