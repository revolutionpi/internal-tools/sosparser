# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText: 2024 KUNBUS GmbH <support@kunbus.com>
# SPDX-License-Identifier: GPL-2.0-or-later
"""Entry point of SOS Parser."""
from flask import render_template, request

from . import app
from .. import __version__


@app.route("/", methods=["GET", "POST"])
def home():
    return render_template(
        "home.html",
    )


@app.route("/info")
def info():
    return render_template(
        "info.html",
        version=__version__,
        storage_directory=app.config["STORAGE_DIRECTORY"],
    )
