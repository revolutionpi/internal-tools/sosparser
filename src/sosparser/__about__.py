# -*- coding: utf-8 -*-
"""Metadata of package."""
# SPDX-FileCopyrightText: 2023 KUNBUS GmbH
# SPDX-License-Identifier: GPL-2.0-or-later
__author__ = "Sven Sager"
__copyright__ = "Copyright (C) 2023 KUNBUS GmbH"
__license__ = " GPL-2.0-or-later"
__version__ = "0.0.1"
