# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText: 2024 KUNBUS GmbH <support@kunbus.com>
# SPDX-License-Identifier: GPL-2.0-or-later
"""Run flask app on a local computer."""
from os import environ
from pathlib import Path

from . import proginit as pi


def main() -> int:
    """Run flask app on a local computer"""
    pi.logger.info("Starting flask")

    # Create Data directory
    user_home = Path.home()
    app_home = user_home.joinpath("SOS-Parser")
    app_home.mkdir(parents=True, exist_ok=True)
    pi.logger.info(f"Storage directory path: {app_home}")

    # Set Flask app environment values
    environ["MY_STORAGE_DIRECTORY"] = str(app_home)

    from .flaskapp import app

    if not pi.pargs.no_browser:
        from webbrowser import open

        open("http://127.0.0.1:5000")

    app.run(
        debug=pi.pargs.debug,
    )

    pi.logger.info("Flask app stopped")
    return 0
