# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText: 2024 KUNBUS GmbH <support@kunbus.com>
# SPDX-License-Identifier: GPL-2.0-or-later
"""WSGI file for flask application."""
from logging import basicConfig
from os import environ
from sys import stderr

basicConfig(stream=stderr)


def application(wsgi_environ, start_response):
    # Set all Apache SetEnv MY_* values to environment
    for key in wsgi_environ:  # type: str
        if key.startswith("MY_"):
            environ[key] = wsgi_environ[key]

    # Import this after preparing the environment. Objects will use the values on import.
    from .flaskapp import app

    return app(wsgi_environ, start_response)
