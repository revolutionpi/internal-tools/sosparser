# -*- coding: utf-8 -*-
"""Package: sosparser."""
# SPDX-FileCopyrightText: 2023 KUNBUS GmbH
# SPDX-License-Identifier: GPL-2.0-or-later
from .__about__ import __author__, __copyright__, __license__, __version__
