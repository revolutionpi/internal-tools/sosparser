SHELL        := bash
MAKEFLAGS     = --no-print-directory --no-builtin-rules
.DEFAULT_GOAL = all

# Variables
PACKAGE = sosparser
APP_NAME = SOS-Parser
APP_IDENT = com.kunbus.revolution.sos-parser

# Set path to create the virtual environment with package name
ifdef PYTHON3_VENV
VENV_PATH = $(PYTHON3_VENV)/$(PACKAGE)
else
VENV_PATH = venv
endif

# If virtualenv exists, use it. If not, use PATH to find commands
SYSTEM_PYTHON  = python3
PYTHON         = $(or $(wildcard $(VENV_PATH)/bin/python), $(SYSTEM_PYTHON))

APP_VERSION = $(shell "$(PYTHON)" src/$(PACKAGE) --version | cut -d ' ' -f 2)

all: test build

.PHONY: all

## Environment
venv-info:
	@echo Environment for $(APP_NAME) $(APP_VERSION)
	@echo Using path: "$(VENV_PATH)"
	exit 0

venv:
	# Start with empty environment
	"$(SYSTEM_PYTHON)" -m venv "$(VENV_PATH)"
	source "$(VENV_PATH)/bin/activate" && \
		python3 -m pip install --upgrade pip && \
		python3 -m pip install -r requirements.txt
	exit 0

venv-ssp:
	# Include system installed site-packages and add just missing modules
	"$(SYSTEM_PYTHON)" -m venv --system-site-packages "$(VENV_PATH)"
	source "$(VENV_PATH)/bin/activate" && \
		python3 -m pip install --upgrade pip && \
		python3 -m pip install -r requirements.txt
	exit 0

.PHONY: venv-info venv venv-ssp

## Build steps
test:
	PYTHONPATH=src "$(PYTHON)" -m pytest

build:
	"$(PYTHON)" -m setup sdist
	"$(PYTHON)" -m setup bdist_wheel

install: build
	"$(PYTHON)" -m pip install dist/$(PACKAGE)-$(APP_VERSION)-*.whl

uninstall:
	"$(PYTHON)" -m pip uninstall --yes $(PACKAGE)

.PHONY: test build install uninstall

## PyInstaller
app-licenses:
	mkdir -p dist
	# Create a list of all installed libraries, their versions and licenses
	"$(PYTHON)" -m piplicenses \
		--format=markdown \
		--output-file dist/bundled-libraries.md
	# Create a list of installed libraries with complete project information
	"$(PYTHON)" -m piplicenses \
		--with-authors \
		--with-urls \
		--with-description \
		--with-license-file \
		--no-license-path \
		--format=json \
		--output-file dist/open-source-licenses.json
	"$(PYTHON)" -m piplicenses \
		--with-authors \
		--with-urls \
		--with-description \
		--with-license-file \
		--no-license-path \
		--format=plain-vertical \
		--output-file dist/open-source-licenses.txt

app: app-licenses
	"$(PYTHON)" -m PyInstaller -n $(APP_NAME) \
		--add-data="dist/bundled-libraries.md:$(PACKAGE)/open-source-licenses" \
		--add-data="dist/open-source-licenses.*:$(PACKAGE)/open-source-licenses" \
		--add-data="src/sosparser/flaskapp/static:$(PACKAGE)/flaskapp/static" \
        --add-data="src/sosparser/flaskapp/templates:$(PACKAGE)/flaskapp/templates" \
		--noconfirm \
		--clean \
		--onefile \
		src/$(PACKAGE)/__main__.py

.PHONY: app-licenses app

## Clean
clean:
	# PyTest caches
	rm -rf .pytest_cache
	# Build artifacts
	rm -rf build dist src/*.egg-info
	# PyInstaller created files
	rm -rf *.spec

distclean: clean
	# Virtual environment
	rm -rf "$(VENV_PATH)"

.PHONY: clean distclean
